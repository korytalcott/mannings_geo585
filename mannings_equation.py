# -*- coding: utf-8 -*-
"""
Created on Mon Apr  6 11:17:26 2020

@author: kt150714
"""
import numpy as np

class ManningsEquation(object):

    #  *Static contstants!!*
    # These are defined outside the constructor __init__
    # and belong to the class. No copy of static variables is 
    # made when you create an object, all objects share the same copy of 

#    S = 0.025 # slope (unitless)
#    n = 0.03 # Mannings number minimum= 0.025 normal= 0.03 maximum= 0.033 (for clean, straight, channel)
#    w = 2.0 # rectangular streambed width (m)
#    fd = 1. # initial guess depth (m)
#
#    Q = np.array([0.1, 0.2, 0.5, 1., 1.75, 1.7, 1.6, 1.4, 1.2, 1., 0.8, 0.6]) # (cumecs)

    def __init__(self,Q):
        """Solves for velocity and flow depth for a clean, rectangular, straight stream channel
        Uses Newton Raphsom method varying bankfull depth to find roots of velocity function.
        
        Parameters:
        -------
        Q: streamflow discharge (cumecs)
        
        Returns
        -------
        None."""
        
        self.Q = Q
    
        # Create lists to store generated values
        self.velocity = []  # (m/s) velocity of streamflow
        self.flow_depth = []  # Flow depth
        self.manningeq_errors = []  # solutions to solve function boundaries
        self.Qs = []  # time step streamflow discharge
        self.dQ = [] # dQdy Derivative of discharge wrt flow depth
        
    def _streamflow_discharge(self, S, n, w, y):
        """Calculates streamflow discharge Qs (cumecs) for a clean, rectangular, straight stream channel.
        
        :input/boundary condition S: Slope of channel (unitless)
        :input/boundary condition n: Manning's number for channel (s/m^(1/3))
        :input/boundary condition w: channel width (m)
        :state variable d: Depth of streamflow (m)
        :returns: calculated discharge Qs"""
    
        Qs = ((((y*w)/(2.*y+w))**(2/3)*np.sqrt(S))/n)*y*w # M_errors discharge (cumecs)
        self.Qs.append(Qs)  
        
        return Qs

    def _dstreamflow_discharge(self, S, n, w, y):
        """Calculates derivative of discharge Q with respect to y flow depth (cumecs).
        
        :input/boundary condition S: Slope of channel (unitless)
        :input/boundary condition n: Manning's number for stream (s/m^(1/3))
        :input/boundary condition w: channel width (m)
        :state variable d: Depth of streamflow (m)
        :returns: dQdy  Qc"""
        
        Qi = self.Qs[-1]
        dQdy = Qi*((6*y+5*w)/(3*y*(2*y+w))) # derivative calculation with respect to y
        self.dQ.append(dQdy)
        
        return dQdy

    def _solve_ManningsEquation(self, S, n, w):
        
        old_depth = 1
        #  Initiates flow depth with last solution of flow depth if there is any, otherwise 0
        y = old_depth
        Meq_err = np.array([1])
        i = 0
        
        # Start Newton-Raphson loop. Stop when Mannings error is smaller than 1e-6
        # or number of iterations exceeds 500
        while (abs(Meq_err) >= 1e-6).any() and ( i < 500):
            Meq_err = self._streamflow_discharge(w, S, n, y) - self.Q
            dMeq_err = self._dstreamflow_discharge(w, S, n, y)
            # Newton Raphson method to update depth
            y = y - (Meq_err/dMeq_err) # <--This is the line of code that does the work
            i += 1
            
        v = self.Q/(w*y)
        self.velocity.append(v)
        self.flow_depth.append(y)
        
        return Meq_err
    
    def run_ManningsEquation(self, S, n, w):
        """Solution to Mannings Equation with the given channel properties

        :input/boundary condition w: channel width (m)
        :input/boundary condition S: Slope of channel (unitless)
        :input/boundary condition n: Mannings number for stream (s/m^(1/3))
        :returns: numpy array with flow depths for each time step of streamflow discharge
        """
        M_err = self._solve_ManningsEquation(S, n, w)
        self.manningeq_errors.append(M_err)
        return self.flow_depth, self.manningeq_errors


#    model = Mannings(Q)
#    output = model.run_Mannings(w,S,n,fd)
#    flow_depth = output[0]
#    Mequation_errors = output[1]
#
#    print (flow_depth)