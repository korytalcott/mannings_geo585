# -*- coding: utf-8 -*-
"""
Created on Sat Apr  4 16:33:18 2020

@author: kt150714
"""

import mannings_equation as ME
import nose

class TestManningEquation(object):
    
    @classmethod
    def setup_class(cls):
        """this method is run once for each class before any tests are run"""
        cls.S = 0.025 #unitless
        cls.n = 0.03
        cls.w = 2. #m
        cls.y = 1.
        
        
    @classmethod
    def teardown_class(cls):
        """this method is run once for each class __after__ all tests are run"""
        pass
    
    def setUp(self):
        """this method is run once before __each__ tests is run"""
        pass
    
    def teardown(self):
        """this method is run once after __each__ tests is run"""
        pass
    
    
    def test_init(self):
        
        manning_eq = ME.ManningsEquation(self)
        
        for i in range(4):
            manning_eq.flow_depth.append(i+1) #make sure I created an appendable list
            
        nose.tools.assert_equal(manning_eq.flow_depth, [1,2,3,4])
        
    def test_calculate_discharge(self):
        manning_eq = ME.ManningsEquation(self)
        
        S = 0.025
        n = 0.03
        w = 2
        y = 1
        
        Q= manning_eq._streamflow_discharge(S, n, w, y)
        
        expected_value =  6.64036698276408  # m/s
        nose.tools.assert_almost_equal(expected_value, Q, places=8)
        
    def test_dcalculate_discharge(self):
        manning_eq = ME.ManningsEquation(self)
        
        S = 0.025
        n = 0.03
        w = 2
        y = 1
        
        dQ = manning_eq._dstreamflow_discharge(S, n, w, y)
        
        expected_value = 8.85382264368544
        
        nose.tools.assert_almost_equal(expected_value, dQ, places=8)
        
    def test_solve_Mannings(self):        
         # create Mannings Equation object
        manning_eq = ME.ManningsEquation(self)
                
#        Q=6.64036698276408
        S = 0.025
        n = 0.03
        w = 2
        
        M_err = manning_eq._solve_ManningsEquation(S, n, w)
        nose.tools.assert_less_equal(abs(M_err), 1e-6)
#        
    def test_run_Mannings(self):
        # create Mannings_vel object
        manning_eq = ME.ManningsEquation(self)
        
#        Q=6.64036698276408
        S = 0.025
        n = 0.03
        w = 2
        
        depth, err = manning_eq.run_ManningsEquation(S, n, w)
        nose.tools.assert_true((abs(err) <= 1e-6).all())

      
   
