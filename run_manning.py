# -*- coding: utf-8 -*-
"""
Created on Sun Apr  5 21:33:18 2020

@author: kt150714
"""
import numpy as np
import mannings_equation as ME
import matplotlib.pyplot as plt

Q = np.array([0.1, 0.2, 0.5, 1., 1.75, 1.7, 1.6, 1.4, 1.2, 1., 0.8, 0.6]) # Discharge (cumecs)
t = np.array([0.5, 1., 1.5, 2., 2.5, 3., 3.5, 4., 4.5, 5., 5.5, 6.]) # time series steps hours
manningeq_errors_min = np.array([1.11022302e-16, 2.60902411e-15, 3.66373598e-15, 2.44249065e-15,
                                 1.55431223e-15, 1.55431223e-15, 1.77635684e-15, 1.99840144e-15,
                                 2.44249065e-15, 2.44249065e-15, 2.88657986e-15, 3.55271368e-15])
manningeq_errors_max = np.array([6.10622664e-16, 5.77315973e-15, 3.10862447e-14, 2.81996648e-14,
                                 2.04281037e-14, 2.04281037e-14, 2.13162821e-14, 2.37587727e-14,
                                 2.57571742e-14, 2.81996648e-14, 3.07531778e-14, 3.19744231e-14])

S = 0.025 # slope (unitless)
n = 0.033 # Mannings number minimum= 0.025 normal= 0.03 maximum= 0.033 (for clean, straight, channel)
w = 2.0 # rectangular streambed width (m)
fd = 1. #initial guess depth (m)

model = ME.ManningsEquation(Q)
output = model.run_ManningsEquation(S, n, w)
flow_depth = output[0]
manningeq_errors = output[1]

print (manningeq_errors)
print (flow_depth)


Scatterplot = plt.scatter(flow_depth, Q)
plt.title('Streamflow vs. Flow Depth (min & max)', fontweight='bold', fontsize=14)
plt.grid(b=True, which='major', color='#666666', linestyle='-', linewidth=.5)
plt.xlabel('Flow Depth [m]', fontweight='bold', fontsize=12)
plt.ylabel('Streamflow [cumecs]', fontweight='bold', fontsize=12)
#legend1 = plt.legend((flow_depth, Q), loc="lower left", title="Flow Depth")
#plt.add_artist(legend1)
plt.show()
print (plt.figure)

#
#Scatterplot = plt.scatter(flow_depth, Q)
#plt.title('Streamflow vs. Flow Depth (Changing Width)', fontweight='bold', fontsize=14)
#plt.grid(b=True, which='major', color='#666666', linestyle='-', linewidth=.5)
#plt.xlabel('Flow Depth [m]', fontweight='bold', fontsize=12)
#plt.ylabel('Streamflow [cumecs]', fontweight='bold', fontsize=12)
##legend1 = plt.legend((flow_depth, Q), loc="lower left", title="Flow Depth")
##plt.add_artist(legend1)
#plt.show()

#Scatterplot = plt.scatter(flow_depth, manningeq_errors_max)
#plt.title('Mannings Error vs. Flow Depth (min & max)', fontweight='bold', fontsize=14)
#plt.grid(b=True, which='major', color='#666666', linestyle='-', linewidth=.5)
#plt.xlabel('Flow Depth [m]', fontweight='bold', fontsize=12)
#plt.ylabel('Streamflow [cumecs]', fontweight='bold', fontsize=12)
##legend1 = plt.legend((flow_depth, Q), loc="lower left", title="Flow Depth")
##plt.add_artist(legend1)
#plt.show()

#fig = plt.figure(figsize=(11,8))
#plt.title('Streamflow Through Time Series', fontweight='bold', fontsize=16)
#plt.grid(b=True, which='major', color='#666666', linestyle='-', linewidth=.5)
#plt.plot(Q, label = 'Discharge', color='b', linewidth=2, marker='o', markersize=7)
##plt.plot(flow_depth, label = '', linestyle='dashed', color='k')
#plt.xlabel('Series Time Step', fontweight='bold', fontsize=12)
#plt.ylabel('Streamflow [cumecs]', fontweight='bold', fontsize=12)
#plt.legend(loc='best')
#plt.show()